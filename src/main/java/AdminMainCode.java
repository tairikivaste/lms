import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class AdminMainCode {

    Scanner scanner = new Scanner(System.in);
    BookDB bookDB = new BookDB();

    public void addBook() {
        Book book = collectBookInfo();
        try {
            bookDB.addBook(book);
            System.out.println("New book added to the library!");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBook() {
        Book book = collectBookInfo();
        try {
            bookDB.updateBook(book);
            System.out.println("Book updated");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteBook() {
        Book book = collectBookInfo();
        try {
            bookDB.deleteBook(book);
            System.out.println("Book deleted from the library");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private Book collectBookInfo() {
        Book book = new Book();
        System.out.println("Book name: ");
        book.setBookName(scanner.nextLine());
        System.out.println("Author:");
        book.setAuthorName(scanner.nextLine());
        System.out.println("Genre:");
        book.setGenre(scanner.nextLine());

        return book;
    }

    public void takeBook() {
    }

    public void returnBook() {
    }

    public void searchBook() {
        System.out.println("Enter Book ID:");
        String bookId = scanner.nextLine();
        Book book = null;
        try {
           book =  bookDB.searchBook(bookId);
        } catch (SQLException e) {
            System.out.println("Failed To Search: " + e.getMessage());
            e.printStackTrace();
        }
        if(book != null){
            System.out.println("\nBook name: " + book.getBookName() + "\nBook Author: " + book.getAuthorName() +
                    "\nGenre " + book.getGenre() );
        } else {
            System.out.println("Could not find books with id " + bookId);
        }
    }

    public void displayBooks() {
        try {
            ArrayList<Book> books = bookDB.displayBooks();
            for (Book currentBook: books){
                System.out.println(currentBook.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
