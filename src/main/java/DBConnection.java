import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBConnection {
    private static Connection connection;
    private static String user;
    private static String password;
    private static String connectionUrl;

    private DBConnection(){
        getSetAuthenticationInfo();
    }

    private static void getSetAuthenticationInfo() {
        user = "root";
        password = "Marley2018";
        connectionUrl = "jdbc:mysql://" + "localhost:3306/" + "library";

    }

    public static Connection getConnection(){
        try {
            if (user == null ) getSetAuthenticationInfo();
            if (connection == null || connection.isClosed()) {
                connection = DriverManager.getConnection(connectionUrl, user, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void closeConnection(PreparedStatement statement, Connection connection) {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
