import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookDB {
    Connection connection = DBConnection.getConnection();

    public void addBook(Book book) throws SQLException {

        String query = "INSERT INTO books(bookName, authorName, genre) VALUES(?, ?, ?)";
        PreparedStatement statement  = null;

        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, book.getBookName());
            statement.setString(2, book.getAuthorName());
            statement.setString(3, book.getGenre());
            statement.executeUpdate();
        } finally {
            DBConnection.closeConnection(statement, connection);
        }
    }
    public void updateBook(Book book) throws SQLException {

        String query = "UPDATE books SET bookname=?, bookauthor=?, genre=?";
        PreparedStatement statement = null;

        try {
        statement = connection.prepareStatement(query);
            statement.setString(1, book.getBookName());
            statement.setString(2, book.getAuthorName());
            statement.setString(3, book.getGenre());
            statement.executeUpdate();
        } finally {
            DBConnection.closeConnection(statement, connection);
        }
    }
    public void deleteBook(Book book) throws SQLException {
        String query = "DELETE FROM books WHERE bookname=?, authorname=?,  genre=?";
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, book.getBookName());
            statement.setString(2, book.getAuthorName());
            statement.setString(3, book.getGenre());
            statement.executeUpdate();
        } finally {
            DBConnection.closeConnection(statement, connection);
        }
    }

    public Book searchBook(String bookId) throws SQLException {
        Book book = null;
        PreparedStatement statement = null;
        String query = "SELECT * FROM books WHERE id=?";

        try {
            connection = DBConnection.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, Integer.parseInt(bookId));
            ResultSet result = statement.executeQuery();

            if(result.next()){
                book = new Book(result.getInt("id"), result.getString("bookName"),
                        result.getString("authorname"), result.getString("genre"));
            }
        } finally{
            DBConnection.closeConnection(statement, connection);
        }
        return book;

    }

    public ArrayList<Book> displayBooks() throws SQLException {
        ArrayList<Book> book = new ArrayList<>();
        PreparedStatement statement = null;
        String query = "SELECT * FROM books";

        try {
            connection = DBConnection.getConnection();
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();

            while (result.next()){
                book.add(new Book(result.getInt("id"), result.getString("bookName"),
                        result.getString("authorname"), result.getString("genre")));
            }
        } finally {
            DBConnection.closeConnection(statement, connection);
        }

        return book;
    }
}
