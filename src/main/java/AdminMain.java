import java.util.Scanner;

public class AdminMain {

    public static void main(String[] args) {
        AdminMainCode adminMainCode = new AdminMainCode();
        Scanner scanner = new Scanner(System.in);
        String userInput = "";

        while (!userInput.equals("5")){
            System.out.println("Welcome to library, Admin"
            + "\n 1. Add book" + "\n 2. Update book" + "\n 3. Delete book"
            + "\n 4. Take book" + "\n 5. Return book" + "\n 6. Search book"
            + "\n 7. Display books" + "\n 8. Exit");

            userInput = scanner.nextLine();
            switch (userInput){
                case "1":
                    adminMainCode.addBook();
                    break;
                case "2":
                    adminMainCode.updateBook();
                    break;
                case "3":
                    adminMainCode.deleteBook();
                    break;
                case "4":
                    adminMainCode.takeBook();
                    break;
                case "5":
                    adminMainCode.returnBook();
                    break;
                case "6":
                    adminMainCode.searchBook();
                    break;
                case "7":
                    adminMainCode.displayBooks();
                    break;
                case "8":
                default:
                    System.exit(0);

            }
        }


    }
}
